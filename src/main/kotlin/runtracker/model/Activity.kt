package runtracker.model

import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer
import runtracker.facade.ActivityStatus
import java.time.Duration
import java.time.LocalDateTime
import javax.persistence.*

enum class ActivityType {
    RUNNING
}

@Entity
data class Activity(var type: ActivityType,
                    var status: ActivityStatus,

                    @JsonSerialize(using = LocalDateTimeSerializer::class)
                    val timeStarted: LocalDateTime,

                    @OneToMany(targetEntity = ActivityCommand::class, cascade = [CascadeType.ALL])
                    val commands: MutableList<ActivityCommand> = mutableListOf(),

                    @JsonSerialize(using = LocalDateTimeSerializer::class)
                    var timeFinished: LocalDateTime? = null,

                    @OneToMany(targetEntity = DataPoint::class, cascade = [CascadeType.ALL])
                    val dataPoints: MutableList<DataPoint> = mutableListOf()) {

    @Id
    @GeneratedValue
    var id: Long? = null

    fun isActive(): Boolean {
        return status != ActivityStatus.FINISHED
    }

    fun totalDistance(): Double {
        return dataPoints.zipWithNext { a, b -> a.distanceTo(b) }.sum()
    }

    fun totalTime(): String {
        //TODO not taking pauses into account
        val duration = Duration.between(commands.first().dateCreated, commands.last().dateCreated)
        val hours = duration.toHours()
        val minutes = duration.minusHours(hours).toMinutes()
        val seconds = duration.minusHours(hours).minusMinutes(minutes).seconds
        return "${String.format("%02d", hours)}:${String.format("%02d", minutes)}:${String.format("%02d", seconds)}"
    }

}