package runtracker.model

import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer
import java.math.BigDecimal
import java.time.LocalDateTime
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id


@Entity
class DataPoint() {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    var id: Long = 0
    lateinit var lat: BigDecimal
    lateinit var lon: BigDecimal

    @JsonSerialize(using = LocalDateTimeSerializer::class)
    lateinit var dateCreated: LocalDateTime

    var accuracy: Float = -1f

    constructor(lat: BigDecimal, lon: BigDecimal, dateCreated: LocalDateTime, accuracy: Float) : this() {
        this.lat = lat
        this.lon = lon
        this.dateCreated = dateCreated
        this.accuracy = accuracy
    }

    override fun toString(): String {
        return "DataPoint(id=$id, lat=$lat, lon=$lon, dateCreated=$dateCreated)"
    }

    fun distanceTo(other: DataPoint): Double {
        return distanceBetween(lat, other.lat, lon, other.lon)
    }

    private fun distanceBetween(lat1: BigDecimal, lat2: BigDecimal, lon1: BigDecimal, lon2: BigDecimal, el1: Double = 0.0, el2: Double = 0.0): Double {

        val R = 6371 // Radius of the earth

        val latDistance = Math.toRadians((lat2 - lat1).toDouble())
        val lonDistance = Math.toRadians((lon2 - lon1).toDouble())
        val a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + (Math.cos(Math.toRadians(lat1.toDouble())) * Math.cos(Math.toRadians(lat2.toDouble()))
                * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2))
        val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
        var distance = R.toDouble() * c * 1000.0 // convert to meters

        val height = el1 - el2

        distance = Math.pow(distance, 2.0) + Math.pow(height, 2.0)

        return Math.sqrt(distance)
    }

}
