package runtracker.model

import runtracker.facade.ActivityCommandFacade
import runtracker.facade.ActivityStatus
import javax.persistence.Entity
import java.time.LocalDateTime
import javax.persistence.GeneratedValue
import javax.persistence.Id

@Entity
data class ActivityCommand(var status: ActivityStatus, var dateCreated: LocalDateTime) {

    @Id
    @GeneratedValue
    var id: Long? = null

    constructor(commandFacade: ActivityCommandFacade) : this(commandFacade.status, commandFacade.dateCreated)

    override fun toString(): String {
        return "ActivityCommand(status=$status, dateCreated=$dateCreated, id=$id)"
    }

}
