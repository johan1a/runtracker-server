package runtracker.facade

enum class ActivityStatus {
    STARTED,
    PAUSED,
    FINISHED
}