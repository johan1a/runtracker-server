package runtracker.facade

import runtracker.model.ActivityType
import java.time.LocalDateTime

class ActivityFacade(val id: Long,
                     val type: ActivityType,
                     val dateCreated: LocalDateTime,
                     val totalDistance: Double,
                     val totalTime: String)