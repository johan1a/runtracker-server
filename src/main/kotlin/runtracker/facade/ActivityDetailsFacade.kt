package runtracker.facade

import runtracker.model.Activity
import runtracker.model.ActivityCommand
import runtracker.model.ActivityType
import runtracker.model.DataPoint
import java.time.LocalDateTime

class ActivityDetailsFacade(activity: Activity) {
    val id: Long
    val type: ActivityType
    val dateCreated: LocalDateTime
    val totalDistance: Double
    val totalTime: String
    val status: ActivityStatus
    val commands: List<ActivityCommand>
    val timeStarted: LocalDateTime
    val timeFinished: LocalDateTime
    val dataPoints: List<DataPoint>

    init {
        if(activity.id == null){
            id = -1
        }else {
            id = activity.id!!
        }
        type = activity.type
        dateCreated = activity.commands.first().dateCreated
        totalDistance = activity.totalDistance()
        totalTime = activity.totalTime()
        status = activity.status
        commands = activity.commands
        timeStarted = activity.timeStarted
        timeFinished = activity.timeFinished!!
        dataPoints = activity.dataPoints
    }

}