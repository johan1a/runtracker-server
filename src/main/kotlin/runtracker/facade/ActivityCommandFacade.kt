package runtracker.facade

import com.fasterxml.jackson.databind.annotation.JsonSerialize
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer
import runtracker.model.ActivityType
import java.time.LocalDateTime


class ActivityCommandFacade() {
    var type: ActivityType? = null

    lateinit var status: ActivityStatus

    @JsonSerialize(using = LocalDateTimeSerializer::class)
    lateinit var dateCreated: LocalDateTime

    constructor(type: ActivityType? = null,
            status: ActivityStatus,
            dateCreated: LocalDateTime) : this(){
        this.dateCreated = dateCreated
        this.status = status
        this.type = type
    }

    override fun toString(): String {
        return "ActivityCommandFacade(type=$type, status=$status)"
    }

}