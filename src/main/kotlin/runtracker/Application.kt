package runtracker

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.ComponentScan

@SpringBootApplication
@ComponentScan("runtracker")
class Application

fun main(args: Array<String>) {
    SpringApplication.run(Application::class.java, *args)
}
