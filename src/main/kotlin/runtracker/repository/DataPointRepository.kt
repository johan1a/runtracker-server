package runtracker.repository

import org.springframework.data.repository.CrudRepository
import runtracker.model.DataPoint

interface DataPointRepository : CrudRepository<DataPoint, Long>