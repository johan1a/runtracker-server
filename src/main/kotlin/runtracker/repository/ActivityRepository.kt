package runtracker.repository

import org.springframework.data.repository.CrudRepository
import runtracker.facade.ActivityStatus
import runtracker.model.Activity

interface ActivityRepository : CrudRepository<Activity, Long> {

    fun findByStatus(status: ActivityStatus): Activity?

}
