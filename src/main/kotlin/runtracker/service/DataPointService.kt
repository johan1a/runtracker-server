package runtracker.service

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import runtracker.model.DataPoint
import runtracker.repository.DataPointRepository

@Service
@Transactional
class DataPointService(private val dataPointRepository: DataPointRepository) {

    fun findAll(): List<DataPoint> {
        return dataPointRepository.findAll().toList()
    }

}