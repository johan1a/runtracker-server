package runtracker.service

import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import runtracker.facade.ActivityCommandFacade
import runtracker.facade.ActivityDetailsFacade
import runtracker.facade.ActivityFacade
import runtracker.facade.ActivityStatus.*
import runtracker.model.Activity
import runtracker.model.ActivityCommand
import runtracker.model.DataPoint
import runtracker.repository.ActivityRepository

@Service
@Transactional
class ActivityService(private val activityRepository: ActivityRepository) {

    fun execute(commandFacade: ActivityCommandFacade) {
        println("Executing command. current Activity size: ${activityRepository.count()}")
        val status = commandFacade.status
        when {
            status == STARTED -> {
                startActivity(commandFacade)
            }
            status == PAUSED -> {
                pauseActivity(commandFacade)
            }
            status == FINISHED -> {
                finishActivity(commandFacade)
            }
        }
    }

    fun getCurrentActivity(): Activity? {
        return activityRepository.findByStatus(STARTED) ?: activityRepository.findByStatus(PAUSED)
    }

    fun index(): List<ActivityFacade> {
        val activities = activityRepository.findAll().toList()
        return activities.map {
            ActivityFacade(it.id!!,
                    it.type,
                    it.commands.first().dateCreated,
                    it.totalDistance(),
                    it.totalTime()
            )
        }.sortedByDescending { it.id }
    }

    fun saveDataPoint(dataPoint: DataPoint) {
        println("Saving datapoint")
        val activeActivity = getCurrentActivity()
        if (activeActivity != null && activeActivity.isActive()) {
            activeActivity.dataPoints += dataPoint
        } else {
            throw  ProcessingFailedException("No Activity found")
        }
    }

    fun get(id: Long): ActivityDetailsFacade {
        return ActivityDetailsFacade(activityRepository.findOne(id))
    }

    private fun save(activity: Activity) {
        activityRepository.save(activity)
    }

    private fun startActivity(commandFacade: ActivityCommandFacade) {
        val type = commandFacade.type ?: throw ProcessingFailedException("Missing ActivityType")

        val currentActivity = getCurrentActivity()

        if (currentActivity != null) {
            if (currentActivity.status != commandFacade.status) {
                println("Updating activity state to ${commandFacade.status}")
                currentActivity.status = STARTED
                currentActivity.commands += ActivityCommand(commandFacade)
            }
        } else {
            println("Saving new activity")
            save(Activity(type = type,
                    status = STARTED,
                    timeStarted = commandFacade.dateCreated,
                    commands = mutableListOf(ActivityCommand(commandFacade))))
        }
    }

    private fun pauseActivity(commandFacade: ActivityCommandFacade) {
        val activeActivity = getCurrentActivity()
        activeActivity?.let {
            println("Pausing activity")
            it.status = PAUSED
            it.commands += ActivityCommand(commandFacade)
        }
    }

    private fun finishActivity(commandFacade: ActivityCommandFacade) {
        val activeActivity = getCurrentActivity()
        activeActivity?.let {
            println("Finishing activity")
            it.status = FINISHED
            it.timeFinished = commandFacade.dateCreated
            it.commands += ActivityCommand(commandFacade)
        }
    }

}
