package runtracker.service

class ProcessingFailedException(message: String) : Exception(message)
