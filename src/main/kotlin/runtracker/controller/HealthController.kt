package runtracker.controller

import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.http.ResponseEntity
import runtracker.facade.HealthResponse

@RestController
class HealthController() {

    @RequestMapping("/health")
    fun get(): ResponseEntity<HealthResponse> {
        return try {
          ResponseEntity(HealthResponse(true), HttpStatus.OK)
        } catch (e: IllegalStateException) {
            ResponseEntity(HealthResponse(true), HttpStatus.INTERNAL_SERVER_ERROR)
        }
    }

}
