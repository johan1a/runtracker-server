package runtracker.controller

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import runtracker.facade.ActivityCommandFacade
import runtracker.facade.ActivityDetailsFacade
import runtracker.facade.ActivityFacade
import runtracker.model.Activity
import runtracker.service.ActivityService

@RequestMapping("activity")
@RestController
class ActivityController(private val activityService: ActivityService) {

    @GetMapping("/{id}")
    fun get(@PathVariable id: Long): ResponseEntity<ActivityDetailsFacade> {
        return try {
            ResponseEntity(activityService.get(id), HttpStatus.OK)
        } catch (e: IllegalStateException) {
            ResponseEntity(null, HttpStatus.NOT_FOUND)
        }
    }

    @PostMapping
    fun post(@RequestBody activityCommandFacade: ActivityCommandFacade): ResponseEntity<Activity> {
        println("Got activityCommandFacade: $activityCommandFacade")
        activityService.execute(activityCommandFacade)
        return ResponseEntity(null, HttpStatus.CREATED)
    }

    @GetMapping
    fun index(): ResponseEntity<List<ActivityFacade>> {
        return ResponseEntity(activityService.index(), HttpStatus.OK)
    }

}