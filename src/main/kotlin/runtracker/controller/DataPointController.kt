package runtracker.controller

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import runtracker.model.DataPoint
import runtracker.service.ActivityService
import runtracker.service.DataPointService
import runtracker.service.ProcessingFailedException

@RequestMapping("datapoint")
@RestController
class DataPointController(private val dataPointService: DataPointService,
                          private val activityService: ActivityService) {

    @GetMapping
    fun index(): ResponseEntity<List<DataPoint>> {
        println("index")
        val dataPoint = dataPointService.findAll()
        return ResponseEntity(dataPoint, HttpStatus.OK)
    }

    @PostMapping
    fun save(@RequestBody dataPoint: DataPoint): ResponseEntity<DataPoint> {
        println("Got datapoint: $dataPoint")
        return try {
            activityService.saveDataPoint(dataPoint)
            ResponseEntity(null, HttpStatus.CREATED)
        } catch (e: ProcessingFailedException) {
            ResponseEntity(null, HttpStatus.BAD_REQUEST)
        }
    }
}
