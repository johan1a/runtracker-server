package runtracker.service

import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.test.context.junit4.SpringRunner
import runtracker.facade.ActivityCommandFacade
import runtracker.facade.ActivityStatus
import runtracker.model.ActivityType
import java.time.LocalDateTime

@RunWith(SpringRunner::class)
@DataJpaTest
internal class ActivityServiceTest {

    @Autowired
    lateinit var service: ActivityService

    @Test
    fun testActivityStates() {
        assertNull(service.getCurrentActivity())

        val now: LocalDateTime = LocalDateTime.now()

        service.execute(ActivityCommandFacade(ActivityType.RUNNING, ActivityStatus.STARTED, now))

        assertEquals(ActivityStatus.STARTED, service.getCurrentActivity()!!.status)

        service.execute(ActivityCommandFacade(ActivityType.RUNNING, ActivityStatus.PAUSED, now))

        assertEquals(ActivityStatus.PAUSED, service.getCurrentActivity()!!.status)

        service.execute(ActivityCommandFacade(ActivityType.RUNNING, ActivityStatus.STARTED, now))

        assertEquals(ActivityStatus.STARTED, service.getCurrentActivity()!!.status)

        service.execute(ActivityCommandFacade(ActivityType.RUNNING, ActivityStatus.PAUSED, now))
        service.execute(ActivityCommandFacade(ActivityType.RUNNING, ActivityStatus.FINISHED, now))
        assertNull(service.getCurrentActivity())
    }


    @Test
    fun testDateTimes() {

        val now: LocalDateTime = LocalDateTime.now()
        service.execute(ActivityCommandFacade(ActivityType.RUNNING, ActivityStatus.STARTED, now))
        val activity = service.getCurrentActivity()
        activity!!.let {
            assertEquals(now, activity.timeStarted)
        }
    }

    @Test
    fun saveManyParallel() {
        var successes = 0
        val threads: MutableList<Thread> = mutableListOf()
        val times = 100
        repeat(times) {
                val now: LocalDateTime = LocalDateTime.now()
                service.execute(ActivityCommandFacade(ActivityType.RUNNING, ActivityStatus.STARTED, now))
                successes +=1
        }
        threads.forEach { it.join() }
        assertEquals(1, service.index().size)
        assertEquals(times, successes)

    }
}