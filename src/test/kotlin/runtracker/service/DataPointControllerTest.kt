package runtracker.service


import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.http.ResponseEntity
import org.springframework.test.context.junit4.SpringRunner
import runtracker.controller.ActivityController
import runtracker.controller.DataPointController
import runtracker.facade.ActivityCommandFacade
import runtracker.facade.ActivityFacade
import runtracker.facade.ActivityStatus
import runtracker.model.Activity
import runtracker.model.ActivityType
import runtracker.model.DataPoint
import java.math.BigDecimal
import java.time.LocalDateTime

@RunWith(SpringRunner::class)
@DataJpaTest
internal class DataPointControllerTest {

    @Autowired
    lateinit var dataPointController: DataPointController

    @Autowired
    lateinit var activityController: ActivityController

    @Test
    fun testSaveDatapoints() {
        val now = LocalDateTime.now()
        activityController.post(ActivityCommandFacade(type = ActivityType.RUNNING,
                status = ActivityStatus.STARTED,
                dateCreated = now))

        val dataPoint0 = DataPoint(lat = BigDecimal(1.1), lon = BigDecimal(1.2), dateCreated = now, accuracy = 40f)
        val dataPoint1 = DataPoint(lat = BigDecimal(2.1), lon = BigDecimal(2.2), dateCreated = now, accuracy = 40f)
        dataPointController.save(dataPoint0)
        dataPointController.save(dataPoint1)

        val activity1 = getActivity()
        assertEquals(dataPoint0.dateCreated, activity1.dateCreated)
    }

    private fun getActivity(): ActivityFacade {
        val index: ResponseEntity<List<ActivityFacade>> = activityController.index()
        assertEquals(1, index.body.size)
        val activity = index.body[0]
        return activity
    }
}
