package runtracker.controller


import org.junit.Test
import org.junit.runner.RunWith

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.embedded.LocalServerPort
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.test.context.junit4.SpringRunner

import org.assertj.core.api.Assertions.assertThat
import org.junit.Assert.assertEquals
import org.junit.Ignore
import runtracker.facade.ActivityCommandFacade
import runtracker.facade.ActivityStatus
import runtracker.model.Activity
import runtracker.model.ActivityType
import runtracker.service.ActivityService
import java.time.LocalDateTime

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
class ActivityControllerTest {

    @LocalServerPort
    private val port: Int = 0

    @Autowired
    private val restTemplate: TestRestTemplate? = null


    @Autowired
    lateinit var activityService: ActivityService


    @Test
    fun emptyActivities() {
        val baseUrl = "http://localhost:$port"
        assertThat(restTemplate!!.getForObject("$baseUrl/activity", String::class.java))
                .contains("[]")
    }


    @Test
    @Ignore
    fun saveActivity() {
        val baseUrl = "http://localhost:$port"
        val startTime = LocalDateTime.parse("2018-06-05T21:13:14.621049")
        val facade0 = ActivityCommandFacade(type = ActivityType.RUNNING,
                status = ActivityStatus.STARTED,
                dateCreated = startTime)

        // TODO postForObject
        activityService.execute(facade0)

        val activity0: Activity = restTemplate!!.getForObject("$baseUrl/activity/1", Activity::class.java)

        assertEquals(startTime, activity0.timeStarted)
        assertEquals(ActivityStatus.STARTED, activity0.status)

        val finishTime = LocalDateTime.parse("2018-06-05T22:22:14.621049")
        val facade1 = ActivityCommandFacade(status = ActivityStatus.FINISHED,
                dateCreated = finishTime)

        activityService.execute(facade1)
        val activity1: Activity = restTemplate.getForObject("$baseUrl/activity/1", Activity::class.java)
        assertEquals(startTime, activity1.timeStarted)
        assertEquals(finishTime, activity1.timeFinished)
        assertEquals(ActivityStatus.FINISHED, activity1.status)
    }

    @Test
    fun dateSerialization() {
        val baseUrl = "http://localhost:$port"
        val startTimeString = "2018-06-05T21:13:14.621049"
        val startTime = LocalDateTime.parse(startTimeString)
        val facade0 = ActivityCommandFacade(type = ActivityType.RUNNING,
                status = ActivityStatus.STARTED,
                dateCreated = startTime)

        // TODO postForObject
        activityService.execute(facade0)
        val finishTimeString = "2018-06-05T22:22:14.621049"
        val finishTime = LocalDateTime.parse(finishTimeString)
        val facade1 = ActivityCommandFacade(status = ActivityStatus.FINISHED,
                dateCreated = finishTime)

        activityService.execute(facade1)

        val activity: String = restTemplate!!.getForObject("$baseUrl/activity/1", String::class.java)

        assertThat(activity).contains(startTimeString)
        assertThat(activity).contains(finishTimeString)
    }
}
