package runtracker.model

import org.junit.Assert
import org.junit.Test
import runtracker.facade.ActivityStatus
import java.time.LocalDateTime


internal class ActivityTest{

    @Test
    fun testTotalDate(){
        val start = ActivityCommand(status = ActivityStatus.STARTED, dateCreated = LocalDateTime.parse("2018-03-04T14:00:00"))
        val finish = ActivityCommand(status = ActivityStatus.STARTED, dateCreated = LocalDateTime.parse("2018-03-04T14:09:04"))
        val commands = mutableListOf(start, finish)
        val activity = Activity(commands = commands, status = ActivityStatus.FINISHED, type = ActivityType.RUNNING, timeStarted = commands.first().dateCreated)
        Assert.assertEquals("9.04", String.format("%.2f", 9.04))
        Assert.assertEquals("00:09:04", activity.totalTime())
    }

}