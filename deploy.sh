#!/bin/sh
ssh -o StrictHostKeyChecking=no -i /var/jenkins_home/.ssh_host/id_rsa johan@moderskeppet 'docker pull johan1a/runtracker-server:latest'
ssh -o StrictHostKeyChecking=no -i /var/jenkins_home/.ssh_host/id_rsa johan@moderskeppet 'docker stop runtracker-server'
ssh -o StrictHostKeyChecking=no -i /var/jenkins_home/.ssh_host/id_rsa johan@moderskeppet 'docker run -d --rm --net=host --name runtracker-server johan1a/runtracker-server:latest'

