def label = "worker-${UUID.randomUUID().toString()}"

podTemplate(label: label,
  serviceAccount: 'jenkins-master',
  containers: [
      containerTemplate(name: 'docker', image: 'docker', command: 'cat', ttyEnabled: true),
      containerTemplate(name: 'gradle', image: 'gradle', command: 'cat', ttyEnabled: true),
      containerTemplate(name: 'kubectl', image: 'lachlanevenson/k8s-kubectl:v1.8.8', command: 'cat', ttyEnabled: true),
],
volumes: [
  hostPathVolume(mountPath: '/var/run/docker.sock', hostPath: '/var/run/docker.sock')
]) {
  node(label) {
    def myRepo = checkout scm
    def gitCommit = myRepo.GIT_COMMIT

    stage('Run unit tests') {
      sh "./gradlew test"
    }

    stage('Build jar') {
      sh './gradlew clean build bootRepackage'
      sh 'cp build/libs/runtracker-server-*.jar docker_image/app.jar'
    }

    stage('Build and push Docker images') {
      container('docker') {
      withCredentials([[$class: 'UsernamePasswordMultiBinding',
                           credentialsId: 'docker-hub-credentials',
                           usernameVariable: 'DOCKER_HUB_USER',
                           passwordVariable: 'DOCKER_HUB_PASSWORD']]) {
          sh """
            docker login -u ${DOCKER_HUB_USER} -p ${DOCKER_HUB_PASSWORD}
            docker build -t johan1a/runtracker-server:${gitCommit} docker_image
            docker build -t johan1a/runtracker-server:latest docker_image
            docker push johan1a/runtracker-server:${gitCommit}
            docker push johan1a/runtracker-server:latest
            """
        }
      }
    }

    stage('Deploy') {
      container('kubectl') {
        sh "kubectl set image deployment runtracker-server runtracker-server=johan1a/runtracker-server:${gitCommit}"
      }
    }
  }
}
